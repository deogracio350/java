import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
	//javax.awt.event.*;


public class TerminosCondisiones extends JFrame implements ActionListener,ChangeListener{

private JCheckBox check1;
private JButton boton1;
private JLabel label1;

public TerminosCondisiones(){

setLayout(null);

	label1 = new JLabel("Debe aceptar los Terminos");
	label1.setBounds(10,10,100,30);
	add(label1);
	
	check1 = new JCheckBox("Acepto");
	check1.setBounds(10,50,200,30);
	check1.addChangeListener(this);
	add(check1);

	boton1 = new JButton("cerrar");
	boton1.setBounds(10,100,100,20);
	add(boton1);
	boton1.addActionListener(this);
	boton1.setEnabled(false);

}
public void stateChanged(ChangeEvent e){
if (check1.isSelected()==true){
boton1.setEnabled(true);

}
else{

boton1.setEnabled(false);
}

}
public void actionPerformed(ActionEvent e){
	if(e.getSource()==boton1){
	System.exit(0);

}


}
public static void main(String args[]){

	TerminosCondisiones formulario = new TerminosCondisiones();
	formulario.setBounds(100,100,400,400);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);



}
}
