import javax.swing.*;
import javax.swing.event.*;

public class Radio extends JFrame implements ChangeListener{
	
	private JRadioButton radio1,radio2,radio3;

	private ButtonGroup bg;

	public Radio(){

	setLayout(null);
	
	bg = new ButtonGroup();
	
	radio1 = new JRadioButton("300*300");
	radio1.setBounds(10,20,100,30);
	radio1.addChangeListener(this);
	add(radio1);
	bg.add(radio1);

	radio2 = new JRadioButton("700*700");
	radio2.setBounds(10,50,100,30);
	radio2.addChangeListener(this);
	add(radio2);
	bg.add(radio2);

	radio3 = new JRadioButton("1024*300");
	radio3.setBounds(10,80,100,30);
	radio3.addChangeListener(this);
	add(radio3);
	bg.add(radio3);

	}

public void stateChanged(ChangeEvent a){

	if(radio1.isSelected()){
	setSize(300,300);
	}
	if(radio2.isSelected()){
	setSize(700,700);
	}
	if(radio3.isSelected()){
	setSize(1024,300);
	}

	}
public static void main(String args[]){

	Radio formulario = new Radio();
	formulario.setBounds(10,100,400,400);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);


}

}
