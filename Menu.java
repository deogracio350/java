import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class Menu extends JFrame implements ActionListener{
	
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuitem1,menuitem2,menuitem3;

	public Menu(){

	setLayout(null);

	menubar = new JMenuBar();
	setJMenuBar(menubar);

	menu = new JMenu("Opcion");
	menubar.add(menu);
	

	menuitem1 = new JMenuItem("Rojo");
	menuitem1.addActionListener(this);	
	menu.add(menuitem1);
	
	menuitem2 = new JMenuItem("Verde");
	menuitem2.addActionListener(this);
	menu.add(menuitem2);

	menuitem3 = new JMenuItem("Azul");
	menuitem3.addActionListener(this);
	menu.add(menuitem3); 
	
	}

	public void actionPerformed(ActionEvent e){
	
	Container fondo = this.getContentPane();
	
	if(e.getSource()== menuitem1){
	fondo.setBackground(new Color(255,0,0));
	

	}
	if(e.getSource()== menuitem2){

		fondo.setBackground(new Color(0,255,0));

	}
	if(e.getSource()== menuitem3){

	fondo.setBackground(new Color(0,0,255));
	

	}
	
	}
	public static void main(String args[]){
	
	Menu formulario = new Menu(); 
	formulario.setBounds(100,100,400,300);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);

	}
	}
