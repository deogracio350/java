import javax.swing.*;
import java.awt.event.*;

public class Combo extends JFrame implements ItemListener{

private JComboBox combo;

public Combo(){

	setLayout(null);

	combo = new JComboBox();
	combo.setBounds(10,10,80,20);
	add(combo);

	combo.addItem("rojo");
	combo.addItem("azul");
	combo.addItem("verde");
	combo.addItemListener(this);
	}
public void itemStateChanged(ItemEvent e){

	if(e.getSource()==combo){

		String valor = combo.getSelectedItem().toString();
		setTitle(valor);
		}


	}
public static void main(String args[]){

	Combo formulario = new Combo();
	formulario.setBounds(0,0,200,50);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);
	}

}
