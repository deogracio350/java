import javax.swing.*;
import java.awt.event.*;

public class Boton_dos extends JFrame implements ActionListener{

	private JButton boton1,boton2,boton3;
	private JLabel label1;

	public Boton_dos(){

	setLayout(null);

	boton1 = new JButton("1");
	boton1.setBounds(0,100,90,30);	
	add(boton1);
	boton1.addActionListener(this);


	boton2 =new JButton("2");
	boton2.setBounds(100,100,90,30);
	add(boton2);
	boton2.addActionListener(this);

	boton3 =new JButton("3");
	boton3.setBounds(200,100,90,30);
	add(boton3);
	boton3.addActionListener(this);

	label1 = new JLabel("en espera...");
	label1.setBounds(10,10,300,30);
	add(label1);
		
	}

	public void actionPerformed(ActionEvent e){

	if(e.getSource()==boton1){

	label1.setText("boton1");
	}
	if(e.getSource()==boton2){

	label1.setText("boton2");
	}
	if(e.getSource()==boton3){

	label1.setText("boton3");
	}
	}

	public static void main(String args[]){

	Boton_dos formulario1 = new Boton_dos();
	formulario1.setBounds(0,0,500,300);
	formulario1.setVisible(true);
	formulario1.setResizable(false);
	formulario1.setLocationRelativeTo(null);


}

}
