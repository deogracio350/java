import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SubMenus extends JFrame implements ActionListener{

	private JMenuBar menubar;
	private JMenu menu1,menu2,menu3;
	private JMenuItem menuitem1,menuitem2,submenuitem1,submenuitem2;

	public SubMenus(){

	setLayout(null);
	
	menubar = new JMenuBar();
	setJMenuBar(menubar);

	menu1 = new JMenu("Opcion");
	menubar.add(menu1);

	menu2 = new JMenu("Tamaño de Ventana");
	menu1.add(menu2);

	menu3 = new JMenu("Color de Ventana");
	menu1.add(menu3);

	submenuitem1 = new JMenuItem("300*300");
	menu2.add(submenuitem1);
	submenuitem1.addActionListener(this);

	submenuitem2 = new JMenuItem("700*700");
	menu2.add(submenuitem2);
	submenuitem2.addActionListener(this);

	menuitem1 = new JMenuItem("azul");
	menu3.add(menuitem1);
	menuitem1.addActionListener(this);
	
	menuitem2 = new JMenuItem("rojo");
	menu3.add(menuitem2);
	menuitem2.addActionListener(this);
		
	
	}

	public void actionPerformed(ActionEvent e){

	Container fondo = this.getContentPane();

	if(e.getSource() == submenuitem1 ){
		setSize(300,300);
	
	}
	if(e.getSource() == submenuitem2 ){
		setSize(700,700);
	
	}
	if(e.getSource()== menuitem2){

		fondo.setBackground(new Color(255,0,0));

	}
	if(e.getSource()== menuitem1){

	fondo.setBackground(new Color(0,0,255));
	

	}


	}

	public static void main(String args[]){

	SubMenus formulario = new SubMenus();
	formulario.setBounds(100,100,500,400);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);	
	
	}
	}
