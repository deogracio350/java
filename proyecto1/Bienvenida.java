import javax.swing.*;
import java.awt.event.*;
import java.awt.*;


public class Bienvenida extends JFrame implements ActionListener{

private JLabel label1,label2,label3,label4;
private JTextField texto1;
private JButton boton1;
public static String nombre = "";
public Bienvenida(){

	setLayout(null);
	setTitle("Bienvenido");	
	getContentPane().setBackground(new Color(255,0,0));
	setIconImage(new ImageIcon(getClass().getResource("images/icon.png")).getImage());

	ImageIcon imagen = new ImageIcon("images/logo-coca.png");	
	label1 = new JLabel(imagen);
	label1.setBounds(25,15,300,150);
	add(label1);
	
	label2 = new JLabel("Sistema de Control Vacasional");
	label2.setBounds(35,135,300,30);
	label2.setFont(new Font("Andale Mono",3,18));
	label2.setForeground(new Color(255,255,255));		
	add(label2);

        label3 = new JLabel("Ingrese su nombre");
	label3.setBounds(45,212,200,30);
	label3.setFont(new Font("Andale Mono",3,18));
	label3.setForeground(new Color(255,255,255));		
	add(label3);

	label4 = new JLabel("©2018  the Coca-Cola Company");
	label4.setBounds(45,440,255,25);
	label4.setFont(new Font("Andale Mono",3,18));
	label4.setForeground(new Color(255,255,255));		
	add(label4);	
	

	texto1 = new JTextField();
	texto1.setBounds(45,240,255,25);
	texto1.setBackground(new Color(224,224,224));
	texto1.setFont(new Font("Andale Mono",2,12));	
	texto1.setForeground(new Color(255,0,0));
	add(texto1);

	boton1 = new JButton("Ingresar");
	boton1.setBounds(125,280,100,30);
	// boton1.setEnable(false);
        boton1.setFont(new Font("Andale Mono",2,12));	
	boton1.setForeground(new Color(255,0,0));
	boton1.addActionListener(this);
	add(boton1);		
}
public void actionPerformed(ActionEvent e){
	if(e.getSource() == boton1){
	nombre = texto1.getText().trim();
	if(nombre.equals("")){
	JOptionPane.showMessageDialog(null, "Debe Ingresar el nombre");
	
	}
	else{
	Licencia licencia =new Licencia();
	licencia.setBounds(100,100,600,400);
	licencia.setVisible(true);
	licencia.setResizable(false);
	licencia.setLocationRelativeTo(null);
	this.setVisible(false);
	}	
	}



}

public static void main(String args[]){

	Bienvenida formulario = new Bienvenida();
	formulario.setBounds(100,100,400,550);
	formulario.setVisible(true);
	formulario.setResizable(false);
	formulario.setLocationRelativeTo(null);



}
}
