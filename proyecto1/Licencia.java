import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import javax.swing.event.*;
public class Licencia extends JFrame implements ActionListener,ChangeListener{

	private JButton boton1,boton2;
	private JTextArea textarea1;
	private JLabel label1,label2;
	private JCheckBox checkbox;
	private JScrollPane scrollpane1;
	String nombre1 = "";

	public Licencia(){

	setLayout(null);

	setTitle("Terminos y Condiciones");
	setIconImage(new ImageIcon(getClass().getResource("images/icon.png")).getImage());

	Bienvenida textobienvenida = new Bienvenida();
	nombre1 = textobienvenida.nombre;

	ImageIcon imagen = new ImageIcon("images/coca-cola.png");	
	label1 = new JLabel(imagen);
	label1.setBounds(270,240,300,150);
	add(label1);
	
	label2 = new JLabel("Terminos y Condiciones");
	label2.setBounds(215,5,200,30);
	label2.setFont(new Font("Andale Mono",1,14));
	label2.setForeground(new Color(0,0,0));
	add(label2);

	textarea1 = new JTextArea();
	textarea1.setFont(new Font("Andale Mono",2,14));
	textarea1.setText("\n\n          TERMINOS Y CONDICIONES\n\n");
	scrollpane1 = new JScrollPane(textarea1);
	scrollpane1.setBounds(10,40,575,200);
	add(scrollpane1);

	boton1 = new JButton("Aceptar");
	boton1.setBounds(10,350,100,30);
	boton1.setEnabled(false);
	boton1.addActionListener(this);	
	add(boton1);

	boton2 = new JButton("No Acepto");
	boton2.setBounds(150,350,100,30);
	boton2.setEnabled(true);
	boton2.addActionListener(this);	
	add(boton2);

	checkbox = new JCheckBox("Yo "+nombre1+" Acepto");
	checkbox.setBounds(10,270,300,30);
	checkbox.addChangeListener(this);	
	add(checkbox);
	}

	public void actionPerformed(ActionEvent e){
	
	if(e.getSource() == boton2){

	System.exit(0);
	}
	if(e.getSource() == boton1){

	System.exit(0);
	}

}

	public void stateChanged(ChangeEvent e){
	if(checkbox.isSelected()==true){
	boton1.setEnabled(true);
	boton2.setEnabled(false);
	}
	if(checkbox.isSelected()==false){
	boton1.setEnabled(false);
	boton2.setEnabled(true);
	}


}
	public static void main(String args[]){

	Licencia licencia =new Licencia();
	licencia.setBounds(100,100,600,400);
	licencia.setVisible(true);
	licencia.setResizable(false);
	licencia.setLocationRelativeTo(null);

}
}
