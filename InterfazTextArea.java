
import javax.swing.*;
import java.awt.event.*;


public class InterfazTextArea extends JFrame implements ActionListener{
	

	private JLabel label1;
	private JTextField textfield1;
	private JTextArea textArea1; 
	private JButton boton1,boton2;

	public InterfazTextArea(){

	setLayout(null);

	label1 = new JLabel("usuario:");
	label1.setBounds(10,10,100,120);
	add(label1);

	textfield1 = new JTextField();
	textfield1.setBounds(20,100,300,30);
	add(textfield1);
	
	textArea1 = new JTextArea();
	textArea1.setBounds(20,150,300,100);
	add(textArea1);	

	boton1 = new JButton("enviar"); 
	boton1.setBounds(20,300,100,50);
	add(boton1);
	boton1.addActionListener(this); 

	boton2 = new JButton("enviar 2"); 
	boton2.setBounds(170,300,100,50);
	add(boton2);
	boton2.addActionListener(this);	

	}

	public void actionPerformed(ActionEvent e){

	if(e.getSource()==boton1){
	String texto = textfield1.getText();
	setTitle(texto);

	}
	if(e.getSource()==boton2){
	String texto = textArea1.getText();
	setTitle(texto);

	}
        
	} 
	public static void main(String args[]){

	InterfazTextArea formulario1 = new InterfazTextArea();
	formulario1.setBounds(100,100,500,500);
	formulario1.setVisible(true);
	formulario1.setResizable(false);
	formulario1.setLocationRelativeTo(null);


	}
	}
